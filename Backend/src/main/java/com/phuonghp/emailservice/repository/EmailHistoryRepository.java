package com.phuonghp.emailservice.repository;

import com.phuonghp.emailservice.entity.EmailHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Transactional
public interface EmailHistoryRepository extends JpaRepository<EmailHistoryEntity, Long> {
    @Modifying
    @Query("UPDATE EmailHistoryEntity SET status = :status WHERE id = :id and status = 'out_going'")
    Integer updateSendingEmailStatus(Long id, String status);

    @Modifying
    @Query("UPDATE EmailHistoryEntity SET status = 'sent', deliveryAt = :deliveryAt WHERE id = :id and status = 'out_going'")
    Integer updateSentEmail(Long id, Timestamp deliveryAt);

    List<EmailHistoryEntity> findBySenderOrderByIdDesc(String sender);

    @Query(value = "SELECT distinct (eh) FROM EmailHistoryEntity eh "
            + "INNER JOIN EmailRecipientEntity er ON eh.id = er.emailHistoryEntity.id "
            + "WHERE er.email = :recipient order by eh.id desc ")
    List<EmailHistoryEntity> getInboxEmails(String recipient);

    @Modifying
    @Query("UPDATE EmailHistoryEntity SET emailClient = :emailClient WHERE id = :id")
    Integer updateEmailClient(Long id, String emailClient);

    @Query(value = "SELECT distinct(eh) FROM EmailHistoryEntity eh "
            + "INNER JOIN EmailRecipientEntity er ON eh.id = er.emailHistoryEntity.id "
            + "WHERE eh.id = :id AND (er.email = :email OR eh.sender = :email)")
    List<EmailHistoryEntity> findByIdAndRelevantEmail(String email, Long id);
}