package com.phuonghp.emailservice.repository;

import com.phuonghp.emailservice.entity.EmailRecipientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRecipientRepository extends JpaRepository<EmailRecipientEntity, Long> {
}