package com.phuonghp.emailservice.service;

import com.phuonghp.emailservice.config.kafka.KafkaCommonConfiguration;
import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.dto.SendEmailRequestDto;
import com.phuonghp.emailservice.emailprovider.EmailServiceProvider;
import com.phuonghp.emailservice.entity.EmailHistoryEntity;
import com.phuonghp.emailservice.exception.CustomException;
import com.phuonghp.emailservice.mapper.EmailHistoryMapper;
import com.phuonghp.emailservice.producer.EmailServiceProducer;
import com.phuonghp.emailservice.repository.EmailHistoryRepository;
import com.phuonghp.emailservice.util.constant.ErrorCodeConstant;
import com.phuonghp.emailservice.util.constant.RecipientConverter;
import com.phuonghp.emailservice.util.constant.RecipientTypeConstant;
import com.phuonghp.emailservice.util.constant.SendEmailStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
@Slf4j
public class EmailService {

    @Autowired
    private EmailHistoryRepository emailHistoryRepository;

    @Autowired
    private EmailServiceProducer emailServiceProducer;

    @Autowired
    private KafkaCommonConfiguration kafkaCommonConfiguration;

    @Autowired
    private EmailServiceProvider emailServiceProvider;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public EmailItemDto sendEmail(SendEmailRequestDto sendEmailRequestDto) {
        EmailHistoryEntity emailHistoryEntity = new EmailHistoryEntity();
        emailHistoryEntity.setContent(sendEmailRequestDto.getContent());
        emailHistoryEntity.setSender(sendEmailRequestDto.getSender());
        emailHistoryEntity.setSubject(sendEmailRequestDto.getSubject());
        emailHistoryEntity.setStatus(SendEmailStatus.OUT_GOING);

        var directRecipients = RecipientConverter.fromListEmailString(sendEmailRequestDto.getDirectRecipients(),
                RecipientTypeConstant.DIRECT.getRecipientType());
        var ccRecipients = RecipientConverter.fromListEmailString(sendEmailRequestDto.getCcRecipients(),
                RecipientTypeConstant.CC.getRecipientType());
        var bccRecipients = RecipientConverter.fromListEmailString(sendEmailRequestDto.getBccRecipients(),
                RecipientTypeConstant.BCC.getRecipientType());
        var recipients = Stream.of(directRecipients, ccRecipients, bccRecipients)
                .flatMap(Collection::stream)
                .map(emailRecipientEntity -> {
                    emailRecipientEntity.setEmailHistoryEntity(emailHistoryEntity);
                    return emailRecipientEntity;
                })
                .collect(Collectors.toList());
        emailHistoryEntity.setRecipients(recipients);
        emailHistoryRepository.save(emailHistoryEntity);
        var emailHistoryItemDto = EmailHistoryMapper.fromEmailHistoryEntity(emailHistoryEntity);
        emailServiceProducer.sendMessage(kafkaCommonConfiguration.getOutgoingEmailTopic(), emailHistoryItemDto.getId().toString(), emailHistoryItemDto);
        return emailHistoryItemDto;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    public void sendEmailAndUpdateStatus(EmailItemDto emailItemDto) {
        var updatedEmailCount = emailHistoryRepository.updateSentEmail(emailItemDto.getId(), new Timestamp(new Date().getTime()));
        if (updatedEmailCount < 1) {
            log.error("Cannot update sending status for email history, email {}", emailItemDto.getId());
        }
        this.sendEmailViaProvider(emailItemDto);
    }

    public void sendEmailViaProvider(EmailItemDto emailItemDto) {
        emailServiceProvider.registerEmailClient();
        var emailClient = emailServiceProvider.getDefaultEmailClient();
        emailServiceProvider.setEmailServiceClient(emailClient);
        emailServiceProvider.setEmailItemDto(emailItemDto);

        var isSuccessful = emailServiceProvider.sendEmail();
        if (Boolean.FALSE.equals(isSuccessful)) {
            emailClient = emailServiceProvider.getFallbackEmailClient();
            if (emailClient != null) {
                emailServiceProvider.setEmailServiceClient(emailClient);
                emailServiceProvider.setEmailItemDto(emailItemDto);
                isSuccessful = emailServiceProvider.sendEmail();
            }
        }
        if (Boolean.TRUE.equals(isSuccessful)) {
            try {
                emailHistoryRepository.updateEmailClient(emailItemDto.getId(), emailClient.getName());
                return;
            } catch (Exception e) {
                log.error("Error occurred when saving client name that send the email", e);
                // Ignore this error due to the email has been sent
                return;
            }
        }
        throw new CustomException.RetryableException("Retry sending email");
    }

    public boolean isValidEmailForSendingViaProvider(EmailItemDto emailItemDto) {
        var emailHistory = emailHistoryRepository.findById(emailItemDto.getId());
        return emailHistory.isPresent() && SendEmailStatus.OUT_GOING.equals(emailHistory.get().getStatus());
    }

    public void updateEmailHistoryStatus(EmailItemDto emailItemDto, String status) {
        var updatedEmailHistoryCount = emailHistoryRepository.updateSendingEmailStatus(
                emailItemDto.getId(), status
        );
        if (updatedEmailHistoryCount > 0) {
            log.error("Updated failed status for error sending email: {}", emailItemDto.getId());
        } else {
            log.error("Cannot update failed status for error sending email");
        }
    }

    public List<EmailItemDto> getSentBox(String sender) {
        var listEmailHistoryEntity = emailHistoryRepository.findBySenderOrderByIdDesc(sender);
        return EmailHistoryMapper.fromListEmailHistoryEntity(listEmailHistoryEntity);
    }

    public List<EmailItemDto> getInbox(String recipient) {
        var listEmailHistoryEntity = emailHistoryRepository.getInboxEmails(recipient);
        return EmailHistoryMapper.fromListEmailHistoryEntity(listEmailHistoryEntity);
    }

    public EmailItemDto getEmailDetails(String email, Long id) {
        var listEmailHistoryEntity = emailHistoryRepository.findByIdAndRelevantEmail(email, id);
        if (!listEmailHistoryEntity.isEmpty()) {
            return EmailHistoryMapper.fromEmailHistoryEntity(listEmailHistoryEntity.get(0));
        }
        throw new CustomException(ErrorCodeConstant.NOT_FOUND, "Not found email");
    }
}
