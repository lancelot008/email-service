package com.phuonghp.emailservice.exception;

import com.phuonghp.emailservice.util.constant.ErrorCodeConstant;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class CustomException extends RuntimeException {
    protected String[] arguments;
    private Integer httpStatus;
    private String errorCode;
    private String message;

    public CustomException(ErrorCodeConstant errorEnum, String message, String... args) {
        super(String.format(errorEnum.getMessage(), (Object[]) args));
        this.httpStatus = errorEnum.getHttpCode();
        this.errorCode = errorEnum.getCode();
        if (message == null || message.isEmpty()) {
            this.message = String.format(errorEnum.getMessage(), (Object[]) args);
        } else {
            this.message = String.format(message, (Object[]) args);
        }
        this.arguments = args;
    }

    public GeneralError transformToRestError() {
        var restError = new GeneralError();
        restError.setCode(errorCode);
        restError.setException(this.getClass().getName());
        return restError;
    }

    public static class RetryableException extends RuntimeException {
        public RetryableException(String message) {
            super(message);
        }
    }
}
