package com.phuonghp.emailservice.exception;

import brave.Tracer;
import com.phuonghp.emailservice.response.GeneralResponse;
import com.phuonghp.emailservice.response.ResponseFactory;
import com.phuonghp.emailservice.util.constant.ErrorCodeConstant;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class APIExceptionHandler {
    @Autowired
    private ResponseFactory responseFactory;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private Tracer tracer;

    @ExceptionHandler(CustomException.class)
    public @ResponseBody
    ResponseEntity<GeneralResponse<Object>> handleAllException(CustomException ex, HttpServletRequest request, HttpServletResponse response) {
        HttpStatus status = ex.getHttpStatus() == null ? HttpStatus.BAD_REQUEST : HttpStatus.valueOf(ex.getHttpStatus());
        this.logException(ex.getMessage(), status.value(), request, ex);
        GeneralError restError = ex.transformToRestError();

        GeneralResponse<Object> resp = new GeneralResponse<>();
        resp.setTraceId(tracer.currentSpan().context().traceIdString());
        resp.setErrorCode(ex.getErrorCode());
        resp.setMessage(restError.getMessage());

        return new ResponseEntity<>(resp, status);
    }

    @ExceptionHandler(Exception.class)
    public @ResponseBody
    ResponseEntity<GeneralResponse<Object>> handleAllException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        this.logException("An unknown error has occurred", ErrorCodeConstant.INTERNAL_SERVER_ERROR.getHttpCode(), request, ex);
        return responseFactory.fail(null, ErrorCodeConstant.INTERNAL_SERVER_ERROR, null);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public @ResponseBody
    ResponseEntity<GeneralResponse<Void>> handleNotReadableException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        this.logException("Not readable exception", ErrorCodeConstant.INVALID_INPUT.getHttpCode(), request, ex);
        return responseFactory.fail(null, ErrorCodeConstant.INVALID_INPUT, null);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ResponseEntity<GeneralResponse<Object>> handleValidationExceptions(
            BindException ex, HttpServletRequest request, HttpServletResponse response) {
        this.logException("Invalid input", ErrorCodeConstant.INVALID_INPUT.getHttpCode(), request, ex);
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String defaultMessage = error.getDefaultMessage();
            errors.put(fieldName, defaultMessage);
        });
        return responseFactory.fail(errors, ErrorCodeConstant.INVALID_INPUT, null);
    }


    private void logException(String msg, int httpStatus, HttpServletRequest request, Exception ex) {
        MDC.put("httpStatus", String.valueOf(httpStatus));
        MDC.put("url", request.getRequestURI());
        log.error(msg, ex);
    }
}
