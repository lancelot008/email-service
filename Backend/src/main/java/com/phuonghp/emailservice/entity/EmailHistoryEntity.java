package com.phuonghp.emailservice.entity;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "email_history", indexes = {
        @Index(name = "idx_emailhistory_sender", columnList = "sender"),
        @Index(name = "idx_emailhistory_sent_at", columnList = "sent_at")
})
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
public class EmailHistoryEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sender")
    private String sender;

    @Column(name = "subject")
    private String subject;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    private String status;

    @CreationTimestamp
    @Column(name = "sent_at")
    private java.sql.Timestamp sentAt;

    @Column(name = "delivery_at")
    private java.sql.Timestamp deliveryAt;

    @UpdateTimestamp
    @Column(name = "last_updated_at")
    private java.sql.Timestamp lastUpdatedAt;

    @Column(name = "email_client")
    private String emailClient;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "emailHistoryEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<EmailRecipientEntity> recipients;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        EmailHistoryEntity that = (EmailHistoryEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

