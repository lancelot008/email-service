package com.phuonghp.emailservice.producer;

import com.phuonghp.emailservice.exception.CustomException;
import com.phuonghp.emailservice.util.constant.ErrorCodeConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@Slf4j
public class EmailServiceProducer {
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    public void sendMessage(String topic, String key, Object message) {
        ListenableFuture<SendResult<String, Object>> future
                = this.kafkaTemplate.send(topic, key, message);

        future.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.debug("Sent message: {} to topic: {} with offset: {}", message, topic, result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                log.error("Unable to send message: {} to topic: {}", topic, message, ex);
                throw new CustomException(ErrorCodeConstant.INTERNAL_SERVER_ERROR, "Cannot publish message");
            }
        });
    }
}
