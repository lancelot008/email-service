package com.phuonghp.emailservice.mapper;

import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.entity.EmailHistoryEntity;
import com.phuonghp.emailservice.util.constant.RecipientConverter;
import com.phuonghp.emailservice.util.constant.RecipientTypeConstant;

import java.util.List;
import java.util.stream.Collectors;

public class EmailHistoryMapper {
    private EmailHistoryMapper() {
    }

    public static EmailItemDto fromEmailHistoryEntity(EmailHistoryEntity emailHistoryEntity) {
        var directRecipients = RecipientConverter.toListEmailString(
                emailHistoryEntity.getRecipients(), RecipientTypeConstant.DIRECT.getRecipientType());
        var ccRecipients = RecipientConverter.toListEmailString(
                emailHistoryEntity.getRecipients(), RecipientTypeConstant.CC.getRecipientType());
        var bccRecipients = RecipientConverter.toListEmailString(
                emailHistoryEntity.getRecipients(), RecipientTypeConstant.BCC.getRecipientType());
        return EmailItemDto.builder()
                .id(emailHistoryEntity.getId())
                .sender(emailHistoryEntity.getSender())
                .subject(emailHistoryEntity.getSubject())
                .content(emailHistoryEntity.getContent())
                .status(emailHistoryEntity.getStatus())
                .directRecipients(directRecipients)
                .ccRecipients(ccRecipients)
                .bccRecipients(bccRecipients)
                .sentAt(emailHistoryEntity.getSentAt())
                .deliveryAt(emailHistoryEntity.getDeliveryAt())
                .emailClient(emailHistoryEntity.getEmailClient())
                .build();
    }

    public static List<EmailItemDto> fromListEmailHistoryEntity(List<EmailHistoryEntity> listEmailHistoryEntity) {
        return listEmailHistoryEntity.stream().map(EmailHistoryMapper::fromEmailHistoryEntity).collect(Collectors.toList());
    }
}
