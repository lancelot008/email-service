package com.phuonghp.emailservice.util.constant;

public class EmailClientConstant {
    private EmailClientConstant() {
    }

    public static final String SENDGRID = "sendgrid";
    public static final String SPARKPOST = "sparkpost";
}
