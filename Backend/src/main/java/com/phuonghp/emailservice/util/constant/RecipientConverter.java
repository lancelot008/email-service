package com.phuonghp.emailservice.util.constant;

import com.phuonghp.emailservice.entity.EmailRecipientEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RecipientConverter {

    private RecipientConverter() {
    }

    public static List<EmailRecipientEntity> fromListEmailString(List<String> recipientEmails, String recipientType) {
        if (recipientEmails == null || recipientEmails.isEmpty()) {
            return new ArrayList<>();
        }
        return recipientEmails
                .stream()
                .map(email -> EmailRecipientEntity
                        .builder()
                        .email(email)
                        .recipientType(recipientType)
                        .build())
                .collect(Collectors.toList());
    }

    public static List<String> toListEmailString(List<EmailRecipientEntity> recipientEmails, String recipientType) {
        if (recipientEmails == null || recipientEmails.isEmpty()) {
            return new ArrayList<>();
        }
        return recipientEmails
                .stream().filter(
                        emailRecipientEntity -> recipientType.equals(emailRecipientEntity.getRecipientType())
                ).map(EmailRecipientEntity::getEmail).collect(Collectors.toList());
    }
}
