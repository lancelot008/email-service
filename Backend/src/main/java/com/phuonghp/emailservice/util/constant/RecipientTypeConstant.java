package com.phuonghp.emailservice.util.constant;

public enum RecipientTypeConstant {
    DIRECT("direct"),
    CC("cc"),
    BCC("bcc");

    private final String recipientType;

    RecipientTypeConstant(String recipientType) {
        this.recipientType = recipientType;
    }

    public String getRecipientType() {
        return recipientType;
    }
}
