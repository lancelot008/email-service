package com.phuonghp.emailservice.util.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorCodeConstant {
    INTERNAL_SERVER_ERROR("internal.server.error", HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal server error"),
    INVALID_INPUT("invalid.input", HttpStatus.BAD_REQUEST.value(), "Invalid input"),
    NOT_FOUND("not.found", HttpStatus.NOT_FOUND.value(), "Data not found"),
    ;

    private final String code;
    private final int httpCode;
    private final String message;

    @Override
    public String toString() {
        return "ResponseStatus{" +
                "code='" + code + '\'' +
                "httpCode='" + httpCode + '\'' +
                '}';
    }
}