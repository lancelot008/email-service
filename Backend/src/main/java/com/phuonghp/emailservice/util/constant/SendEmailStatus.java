package com.phuonghp.emailservice.util.constant;

public class SendEmailStatus {
    public static final String OUT_GOING = "out_going";
    public static final String SENT = "sent";
    public static final String FAILED = "failed";

    private SendEmailStatus() {
    }
}
