package com.phuonghp.emailservice.config.emailclient;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Configuration
@Validated
@ConfigurationProperties(prefix = "emailclient")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class EmailClientConfiguration {
    @NotEmpty(message = "Missing default email client")
    private String defaultEmailClient;
}
