package com.phuonghp.emailservice.config.kafka;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Configuration
@Validated
@ConfigurationProperties(prefix = "kafka")
public class KafkaCommonConfiguration {
    @NotEmpty(message = "Missing bootstrap address for kafka")
    private String bootstrapAddress;
    @NotEmpty(message = "Missing topic for outgoing email")
    private String outgoingEmailTopic;
    @NotEmpty(message = "Missing group id for outgoing email consumer")
    private String outgoingEmailConsumerGroupId;

    @NotEmpty(message = "Missing topic for error sending email")
    private String errorSendingEmailTopic;

    @NotEmpty(message = "Missing group id for error sending email consumer")
    private String errorSendingEmailConsumerGroupId;

    private int backOffInterval = 5000;
    private int maxAttemptOnError = 3;
    private int maxPollRecords = 150;
    private int maxPollInterval = 300000;
    private boolean autoCreateRetryTopic = true;
    private int retryTopicPartition = 3;
    private short retryTopicReplica = 3;
    private int retryDelay = 5 * 1000;
    private double retryDelayMultiplier = 2.0;
    private int maxRetryInterval = 5 * 60 * 1000;
    private int maxAttemptOnRetry = 3;
    private int consumerConcurrency = 1;
    private int producerMaxAttemptOnError = 3;
    private int producerBackOffInterval = 5000;
}
