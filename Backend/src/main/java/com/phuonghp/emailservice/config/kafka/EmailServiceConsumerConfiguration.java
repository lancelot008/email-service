package com.phuonghp.emailservice.config.kafka;

import com.phuonghp.emailservice.exception.CustomException;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.retrytopic.RetryTopicConfiguration;
import org.springframework.kafka.retrytopic.RetryTopicConfigurationBuilder;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.util.backoff.FixedBackOff;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class EmailServiceConsumerConfiguration {
    @Autowired
    private KafkaCommonConfiguration kafkaCommonConfiguration;

    @Bean
    DefaultErrorHandler defaultErrorHandler() {
        return new DefaultErrorHandler((rec, ex) -> {
        }, new FixedBackOff(kafkaCommonConfiguration.getBackOffInterval(), kafkaCommonConfiguration.getMaxAttemptOnError()));
    }

    @Bean
    public ConsumerFactory<String, Object> emailServiceConsumerFactory() {
        JsonDeserializer<Object> deserializer = new JsonDeserializer<>();
        deserializer.addTrustedPackages("*");

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaCommonConfiguration.getBootstrapAddress());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaCommonConfiguration.getOutgoingEmailConsumerGroupId());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, kafkaCommonConfiguration.getMaxPollRecords());
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, kafkaCommonConfiguration.getMaxPollInterval());

        var consumerFactory = new DefaultKafkaConsumerFactory<String, Object>(props);
        consumerFactory.setValueDeserializer(deserializer);

        return consumerFactory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory(
            ConsumerFactory<String, Object> emailServiceConsumerFactory, DefaultErrorHandler defaultErrorHandler) {
        ConcurrentKafkaListenerContainerFactory<String, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(emailServiceConsumerFactory);
        factory.setCommonErrorHandler(defaultErrorHandler);
        factory.setConcurrency(kafkaCommonConfiguration.getConsumerConcurrency());
        factory.getContainerProperties().setDeliveryAttemptHeader(true);
        return factory;
    }

    @Bean
    public RetryTopicConfiguration outgoingEmailRetryableTopic(
            ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory,
            KafkaTemplate<String, Object> kafkaTemplate) {
        return RetryTopicConfigurationBuilder
                .newInstance()
                .autoCreateTopics(
                        kafkaCommonConfiguration.isAutoCreateRetryTopic(),
                        kafkaCommonConfiguration.getRetryTopicPartition(),
                        kafkaCommonConfiguration.getRetryTopicReplica())
                .listenerFactory(kafkaListenerContainerFactory)
                .retryOn(CustomException.RetryableException.class)
                .exponentialBackoff(
                        kafkaCommonConfiguration.getRetryDelay(),
                        kafkaCommonConfiguration.getRetryDelayMultiplier(),
                        kafkaCommonConfiguration.getMaxRetryInterval())
                .maxAttempts(kafkaCommonConfiguration.getMaxAttemptOnRetry())
                .includeTopic(kafkaCommonConfiguration.getOutgoingEmailTopic())
                .suffixTopicsWithIndexValues()
                .create(kafkaTemplate);
    }
}
