package com.phuonghp.emailservice.config.emailclient;

import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.util.constant.EmailClientConstant;
import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Slf4j
@Getter
@Setter
@Configuration
@Validated
@ConfigurationProperties(prefix = "sparkpost")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SparkPostClient implements EmailServiceClient {

    @NotEmpty(message = "Missing api key for Spark Post")
    private String apiKey;
    private Client client;

    @Override
    public Boolean sendEmail(EmailItemDto emailItemDto) {
        try {
            this.client = new Client(this.apiKey);
            this.client.sendMessage(
                    emailItemDto.getSender(),
                    emailItemDto.getDirectRecipients(),
                    emailItemDto.getSubject(),
                    emailItemDto.getContent(),
                    emailItemDto.getContent());
            return true;
        } catch (SparkPostException e) {
            log.info("Error sending email: {}", emailItemDto);
            log.error("Error when sending email via Spark Post", e);
            return false;
        }
    }

    @Override
    public String getName() {
        return EmailClientConstant.SPARKPOST;
    }
}
