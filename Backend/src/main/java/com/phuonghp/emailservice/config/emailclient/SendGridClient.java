package com.phuonghp.emailservice.config.emailclient;

import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.util.constant.EmailClientConstant;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import java.io.IOException;

@Slf4j
@Getter
@Setter
@Configuration
@Validated
@ConfigurationProperties(prefix = "sendgrid")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SendGridClient implements EmailServiceClient {
    @NotEmpty(message = "Missing api key for SendGrid")
    private String apiKey;
    private SendGrid sendGrid;

    @Override
    public Boolean sendEmail(EmailItemDto emailItemDto) {
        this.sendGrid = new SendGrid(this.apiKey);
        Email from = new Email(emailItemDto.getSender());
        String subject = emailItemDto.getSubject();
        Content content = new Content("text/plain", emailItemDto.getContent());

        var personalization = new Personalization();
        for (String directRecipient : emailItemDto.getDirectRecipients()) {
            personalization.addTo(new Email(directRecipient));
        }
        for (String ccRecipient : emailItemDto.getCcRecipients()) {
            personalization.addCc(new Email(ccRecipient));
        }
        for (String bccRecipient : emailItemDto.getBccRecipients()) {
            personalization.addBcc(new Email(bccRecipient));
        }

        Mail mail = new Mail();
        mail.addPersonalization(personalization);
        mail.addContent(content);
        mail.setSubject(subject);
        mail.setFrom(from);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            var response = this.sendGrid.api(request);
            var isSuccess = response.getStatusCode() <= 400;
            if (!isSuccess) {
                log.error("Error when sending email via SendGrid: {}", response.getBody());
            }
            return isSuccess;
        } catch (IOException e) {
            log.info("Error sending email: {}", emailItemDto);
            log.error("Error when sending email via SendGrid", e);
            return false;
        }
    }

    @Override
    public String getName() {
        return EmailClientConstant.SENDGRID;
    }
}
