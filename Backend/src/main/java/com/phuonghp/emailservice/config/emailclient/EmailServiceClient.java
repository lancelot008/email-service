package com.phuonghp.emailservice.config.emailclient;

import com.phuonghp.emailservice.dto.EmailItemDto;

public interface EmailServiceClient {
    Boolean sendEmail(EmailItemDto emailItemDto);
    String getName();
}
