package com.phuonghp.emailservice.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PagingResDto<T> {
    private int totalPages;
    private long totalElements;
    private int size;
    private int number;
    private List<T> content;
}
