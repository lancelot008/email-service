package com.phuonghp.emailservice.response;


import brave.Tracer;
import com.phuonghp.emailservice.util.constant.ErrorCodeConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class ResponseFactory {
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private Tracer tracer;

    public <D> ResponseEntity success(D data) {
        var responseObject = GeneralResponse.createResponse(data);
        responseObject.setSuccess(true);
        return ResponseEntity.ok().body(responseObject);
    }

    public <D> ResponseEntity fail(D data, ErrorCodeConstant code, String message) {
        if (message == null || message.isEmpty()) {
            message = code.getMessage();
        }
        var responseObject = GeneralResponse.createResponse(data);
        responseObject.setErrorCode(code.getCode());
        responseObject.setMessage(message);
        responseObject.setTraceId(tracer.currentSpan().context().traceIdString());
        return ResponseEntity.status(code.getHttpCode()).body(responseObject);
    }
}
