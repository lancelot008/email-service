package com.phuonghp.emailservice.controller;

import com.phuonghp.emailservice.dto.SendEmailRequestDto;
import com.phuonghp.emailservice.response.ResponseFactory;
import com.phuonghp.emailservice.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@RestController
public class EmailServiceController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private ResponseFactory responseFactory;

    @PostMapping("/emails")
    public ResponseEntity sendEmail(@Valid @RequestBody SendEmailRequestDto sendEmailRequestDto) {
        var response = emailService.sendEmail(sendEmailRequestDto);
        return responseFactory.success(response);
    }

    @GetMapping("/emails/inbox")
    public ResponseEntity inbox(@NotEmpty @RequestParam String recipient) {
        var response = emailService.getInbox(recipient);
        return responseFactory.success(response);
    }

    @GetMapping("/emails/sent")
    public ResponseEntity sent(@NotEmpty @RequestParam String sender) {
        var response = emailService.getSentBox(sender);
        return responseFactory.success(response);
    }

    @GetMapping("/emails/{id}")
    public ResponseEntity inbox(@NotEmpty @RequestParam String email, @PathVariable Long id) {
        var response = emailService.getEmailDetails(email, id);
        return responseFactory.success(response);
    }
}
