package com.phuonghp.emailservice.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class LogInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        logRequestStart(request);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) {
        logRequestEnd(request);
    }

    private void logRequestEnd(HttpServletRequest request) {
        log.info("End request: {} {}", request.getMethod(), request.getRequestURI());
    }

    private void logRequestStart(HttpServletRequest request) {
        log.info("Start request: {} {}", request.getMethod(), request.getRequestURI());
    }
}
