package com.phuonghp.emailservice.consumer;

import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.service.EmailService;
import com.phuonghp.emailservice.util.constant.SendEmailStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ErrorSendingEmailConsumer {

    @Autowired
    private EmailService emailService;

    @KafkaListener(topics = "${kafka.errorSendingEmailTopic}",
            groupId = "${kafka.errorSendingEmailConsumerGroupId}",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(@Payload EmailItemDto emailItemDto,
                        @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                        @Header(KafkaHeaders.DELIVERY_ATTEMPT) int attempt) {
        log.info("Consume message {}, from topic {}, attempt {}-th", emailItemDto, topic, attempt);
        emailService.updateEmailHistoryStatus(emailItemDto, SendEmailStatus.FAILED);
    }
}
