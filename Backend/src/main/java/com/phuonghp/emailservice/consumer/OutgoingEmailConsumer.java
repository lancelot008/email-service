package com.phuonghp.emailservice.consumer;

import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class OutgoingEmailConsumer {

    @Autowired
    private EmailService emailService;


    @KafkaListener(topics = "${kafka.outgoingEmailTopic}",
            groupId = "${kafka.outgoingEmailConsumerGroupId}",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(@Payload EmailItemDto emailItemDto,
                        @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                        @Header(KafkaHeaders.DELIVERY_ATTEMPT) int attempt) throws Exception {

        log.info("Consume message {}, from topic {}, attempt {}", emailItemDto, topic, attempt);
        var isValidEmailForSending = emailService.isValidEmailForSendingViaProvider(emailItemDto);
        if (!isValidEmailForSending) {
            log.info("Email status is invalid for sending");
            return;
        }
        emailService.sendEmailAndUpdateStatus(emailItemDto);
    }
}
