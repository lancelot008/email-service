package com.phuonghp.emailservice.emailprovider;

import com.phuonghp.emailservice.config.emailclient.EmailClientConfiguration;
import com.phuonghp.emailservice.config.emailclient.EmailServiceClient;
import com.phuonghp.emailservice.config.emailclient.SendGridClient;
import com.phuonghp.emailservice.config.emailclient.SparkPostClient;
import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.util.constant.EmailClientConstant;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class EmailServiceProvider {
    private EmailServiceClient emailServiceClient;
    private EmailItemDto emailItemDto;

    @Autowired
    private SendGridClient sendGridClient;

    @Autowired
    private SparkPostClient sparkPostClient;

    @Autowired
    private EmailClientConfiguration emailClientConfiguration;

    private Map<String, EmailServiceClient> emailClientRegistrar;

    public Boolean sendEmail() {
        return this.emailServiceClient.sendEmail(this.emailItemDto);
    }

    public void registerEmailClient() {
        this.emailClientRegistrar = new HashMap<>();
        this.emailClientRegistrar.put(EmailClientConstant.SENDGRID, this.sendGridClient);
        this.emailClientRegistrar.put(EmailClientConstant.SPARKPOST, this.sparkPostClient);
    }

    public EmailServiceClient getDefaultEmailClient() {
        return this.emailClientRegistrar.get(emailClientConfiguration.getDefaultEmailClient());
    }

    public EmailServiceClient getFallbackEmailClient() {
        Random r = new Random();
        var fallbackClients = this.emailClientRegistrar.keySet()
                .stream()
                .filter(emailClient -> !emailClientConfiguration.getDefaultEmailClient().equals(emailClient)).collect(Collectors.toList());
        if (!fallbackClients.isEmpty()) {
            int randomItem = r.nextInt(fallbackClients.size());
            String randomElement = fallbackClients.get(randomItem);
            return this.emailClientRegistrar.get(randomElement);
        }
        return null;
    }
}
