package com.phuonghp.emailservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@ToString
public class EmailItemDto implements Serializable {
    private Long id;
    private String sender;
    private String subject;
    private String content;
    private String status;
    private List<String> directRecipients;
    private List<String> ccRecipients;
    private List<String> bccRecipients;
    private Timestamp sentAt;
    private Timestamp deliveryAt;
    private String emailClient;
}
