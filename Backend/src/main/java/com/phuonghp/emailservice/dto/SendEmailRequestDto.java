package com.phuonghp.emailservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendEmailRequestDto implements Serializable {
    @NotEmpty
    @Email
    @Length(max = 64)
    private String sender;
    @Length(max = 255)
    private String subject;
    @NotEmpty
    @Length(max = 4000)
    private String content;
    @Size(min = 1, max = 5)
    @NotEmpty
    private List<@Valid @Size(max = 64)
    @Email(message = "Email is not valid", regexp = "^(?=.{1,64}@)[A-Za-z0-9\\+_-]+(\\.[A-Za-z0-9\\+_-]+)*@"
            + "[^-][A-Za-z0-9\\+-]+(\\.[A-Za-z0-9\\+-]+)*(\\.[A-Za-z]{2,})$")
            String> directRecipients;
    @Size(max = 5)
    private List<@Valid @Size(max = 64)
    @Email(message = "Email is not valid", regexp = "^(?=.{1,64}@)[A-Za-z0-9\\+_-]+(\\.[A-Za-z0-9\\+_-]+)*@"
            + "[^-][A-Za-z0-9\\+-]+(\\.[A-Za-z0-9\\+-]+)*(\\.[A-Za-z]{2,})$")
            String> ccRecipients;
    @Size(max = 5)
    private List<@Valid @Size(max = 64)
    @Email(message = "Email is not valid", regexp = "^(?=.{1,64}@)[A-Za-z0-9\\+_-]+(\\.[A-Za-z0-9\\+_-]+)*@"
            + "[^-][A-Za-z0-9\\+-]+(\\.[A-Za-z0-9\\+-]+)*(\\.[A-Za-z]{2,})$")
            String> bccRecipients;
}
