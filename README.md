# Email Service

## Table of Contents

- [Problem](#problem)
- [Solution](#solution)
    - [Architecture](#architecture)
        - [Modules](#modules)
        - [Technical choices](#technical-choices)
        - [Main functions](#main-functions)
        - [Left out functions](#left-out-functions)
    - [APIs design](#apis-design)
    - [Database design](#database-design)
    - [UI design](#ui-design)
    - [Running and deployment](#running-and-deployment)
- [Deployed services](#deployed-services)
- [Additional](#additional)

## Problem

Create a service that accepts the necessary information and sends emails. It should provide an abstraction between two
different email service providers. If one of the services goes down, your service can quickly failover to a different
provider without affecting your customers.

Example Email Providers:

* [SendGrid](https://sendgrid.com/user/signup)
    - [Simple Send Documentation](https://sendgrid.com/docs/API_Reference/Web_API/mail.html)
* [Mailgun](http://www.mailgun.com)
    - [Simple Send Documentation](http://documentation.mailgun.com/quickstart.html#sending-messages)
* [SparkPost](https://www.sparkpost.com/) - [Developer Hub](https://developers.sparkpost.com/)
* [Amazon SES](http://aws.amazon.com/ses/)
    - [Simple Send Documentation](http://docs.aws.amazon.com/ses/latest/APIReference/API_SendEmail.html)

All listed services are free to try and are pretty painless to sign up for, so please register your own test accounts on
each.

## Solution

The solution focuses on back-end side, with a trivial frontend website. I use message brokers for receive the send email
request from user, and process them in the background. The speed of sending email depends on the quality of email
service provider (like sendgrid, sparkpost,...), but in common, it takes serveral senconds to retrieve the final result
from provider. The actual sending process is in the background so users don't have to wait for the response from server,
and the most important benefit of this solution is we can replay or retry the sending process if some errors occur. This
make our service is fault tolerance and reliable.

### Architecture

![System Architecture {caption=Modules overview}](design/architecture/SystemArchitecture.png)

![Non-blocking retries {caption=Non-blocking retries}](design/architecture/kafka-non-blocking-retries.png)

**Errors trickle down levels of retry topics until landing in the DLT:**

- If message processing fails, the message is forwarded to a retry topic with a back off timestamp.
- The retry topic consumer then checks the timestamp and if it's not due it pauses the consumption for that topic's
  partition.
- When it is due the partition consumption is resumed, and the message is consumed again.
- If the message processing fails again the message will be forwarded to the next retry topic, and the pattern is
  repeated until a successful processing occurs, or the attempts are exhausted,
- If all retry attempts are exhausted the message is sent to the Dead Letter Topic for visibility and diagnosis.
- Dead letter Topic messages can be reprocessed by being published back into the first retry topic. This way, they have
  no influence of the live traffic.

#### Modules

- **Frontend website:** is a trivia site, using pure HTML and JavaScript to craft the UI and request to backend service.

- **Backend service:** The service includes 3 main modules for 3 responsibilities:
    - **Restful API:** handle the request from Frontend website, read/write from database to respond appropriated data.
    - **Message producer:** transform Restful request to a message and push to brokers. In this case, the message is the
      email item that user want to send.
    - **Message consumer:** consume the messages, do some heavy works like request to the email service provider (
      SendGrid, SparkPost,...), fallback to other provider when default provider responds an error. And reconsume the
      messages from retry queue or dead letter queue.
    - These services can be split to separate repos and deploy on difference containers to archive scalability and
      decouple development.

- **Message broker:** is the service for storing and broking the message from producer to consumer. The message brokers
  follow the cluster architect to maintain the high availability for service. This state of cluster is maintain by
  Zookeeper.

- **Database:** for storing the emails and status of sending process.

- **Mail service:** for sending email (SendGrid, SparkPost)

#### Technical choices

- **Frontend website:** based on AdminLTE template, using Nginx to serve the index.html and static files.

- **Backend service:** I choose Spring Boot to build these services. There are some reason for me to make that decision:
  I have experience with Java and Spring Boot; Spring Kafka client is battery-included, well documents, and support my
  solution for the retries and delay processing.

- **Message broker:** I choose Kafka for this module. The main reason is performance of kafka, and the messages can
  persist to storage, and we can organize them to achived the ordering.

- **Database:** I choose MySQL for database for the familiar, and it's performance.

- **Deployment:** I use Docker for packaging and Gitlab CI for shipping the services. Environment variables and
  properties files is kept in Gitlab CI/CD variable and secured files.

- **Email service:** I choose SparkPost and SendGrid, these services provide official SDK for java and many languages.
  But there are some missing features. SparkPost client does not support Cc and Bcc directly,...

#### Main functions

- Access with email to view mail boxes and send email
- Send an email to one or multiple recipients
- View my inbox list and details
- View my sent box list and details

Send email sequence![Send email sequence diagram](design/architecture/SendEmailSequence.png)

#### Left out functions

- **Authentication & authorization:** This is trivial feature that every app requires. However, on this project I focus
to demonstrate the event sourcing and event handling system, and due to the time is not enough for me working on backend also
frontend development, so I choose to left this function out.
- **Pagination for list emails:** same reason as authentication & authorization
- **Email attachment:** Due to the working time is not enough
- **Fetching email history:** only support view history of the emails that sent by this service
- **Some other utilities:** snooze to send, junk box,...
- **Unit & coverage testing:** Due to the working time is not enough (maybe added later)

### APIs design

All APIs have common response format:

**Success response**

```json
{
  "success": true,
  "data": {
  }
}
```

**Error response**

```json
{
  "success": false,
  "errorCode": "invalid.input",
  "traceId": "1b364f644fd06a5c",
  "message": "Invalid input",
  "data": {
    "sender": "must be a well-formed email address"
  }
}
```

**Send email request**

```shell
curl --location --request POST 'localhost:8080/emails' \
--header 'Content-Type: application/json' \
--data-raw '{
    "sender": "phuonghp@testing.com",
    "content": "test",
    "subject": "Backend test",
    "directRecipients": ["phucphuong1008@gmail.com"],
    "ccRecipients": ["test1@gmail.com"],
    "bccRecipients": ["test2@gmail.com"]
}'
```

**Send email response**

```json
{
  "success": true,
  "data": {
    "id": 9,
    "sender": "phuonghp@testing.com",
    "subject": "Backend test",
    "content": "test",
    "status": "out_going",
    "directRecipients": [
      "phucphuong1008@gmail.com"
    ],
    "ccRecipients": [
      "test1@gmail.com"
    ],
    "bccRecipients": [
      "test2@gmail.com"
    ],
    "sentAt": 1668963195146
  }
}
```

**Get inbox emails request**

```shell
curl --location --request GET 'localhost:8080/emails/inbox?recipient=phucphuong1293@gmail.com'
```

**Get inbox emails response**

```json
{
  "success": true,
  "data": [
    {
      "id": 1,
      "sender": "test@gmail.com",
      "subject": "",
      "content": "<p>test</p>",
      "status": "out_going",
      "directRecipients": [
        "phucphuong1293@gmail.com"
      ],
      "ccRecipients": [],
      "bccRecipients": [],
      "sentAt": 1668944883000
    }
  ]
}
```

**Get sent box emails request**

```shell
curl --location --request GET 'localhost:8080/emails/sent?sender=phuonghp@testing.com'
```

**Get sent box emails response**

```json
{
  "success": true,
  "data": [
    {
      "id": 9,
      "sender": "phuonghp@testing.com",
      "subject": "Backend test",
      "content": "test",
      "status": "sent",
      "directRecipients": [
        "phucphuong1008@gmail.com"
      ],
      "ccRecipients": [
        "test1@gmail.com"
      ],
      "bccRecipients": [
        "test2@gmail.com"
      ],
      "sentAt": 1668963195000,
      "deliveryAt": 1668963196000,
      "emailClient": "sparkpost"
    }
  ]
}
```

**Get email details request**

```shell
curl --location --request GET 'localhost:8080/emails/9?email=phuonghp@testing.com'
```

**Get email details response**

```json
{
  "success": true,
  "data": {
    "id": 9,
    "sender": "phuonghp@testing.com",
    "subject": "Backend test",
    "content": "test",
    "status": "sent",
    "directRecipients": [
      "phucphuong1008@gmail.com"
    ],
    "ccRecipients": [
      "test1@gmail.com"
    ],
    "bccRecipients": [
      "test2@gmail.com"
    ],
    "sentAt": 1668963195000,
    "deliveryAt": 1668963196000,
    "emailClient": "sparkpost"
  }
}
```

### Database design

![Database design](design/architecture/DatabaseDesign.png)

- **Table email_history:** store details of email and sender email address
- **Table email_recipient:** store recipient email and type (direct, Cc, Bcc) and relationship of recipient emails with
  email history

### UI design

![Access with email](design/ui/AccessWithEmail.png)

![Compose email](design/ui/ComposeEmail.png)

![List emails](design/ui/ListEmails.png)

### Running and deployment

#### Requirements:

- Linux or Unix -kind OS
- Docker (>=20.10.21) and docker compose (>=v2.12.2)
- Mem >= 4GB
- Storage >= 5GB

#### Steps

- **Step1:** Clone project to local & access to folder of project
- **Step2:** Create .env file in current directory with format like below

```text
MYSQL_ROOT_PASSWORD=<Enter your root password of MySQL for container>
MYSQL_USER=<Enter your username of MySQL for container>
MYSQL_PASSWORD=<Enter your password of MySQL for container
MYSQL_DATABASE=<Enter your database name of MySQL for container
```

- **Step3:** Create application.properties file and move it to `./Backend/src/main/resources/` with this format

```properties
spring.datasource.url=jdbc:mysql://mysql:3306/<enter your mysql database from step 2>?useUnicode=yes&characterEncoding=UTF-8
spring.datasource.password=<enter your mysql password from step 2>
spring.datasource.username=<enter your mysql username from step 2>
# Set it true, if you want liquibase apply the changelog when service is starting
spring.liquibase.enabled=true
spring.liquibase.change-log=classpath:db/changelog/2022/11/18-01-changelog.xml
spring.jpa.show-sql=true
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE
# bootstrap addresses for kafka, if you use the docker-compose file from this project
# you can leave it as the template
kafka.bootstrapAddress=kafka1:19091,kafka2:19092,kafka3:19093
# This is the topic for sending email, you can enter any value for it or just leave it as the template
kafka.outgoingEmailTopic=outgoing-emails
# This is the group id of consumer that consumes the message from topic for sending email
# You can enter any value for it or just leave it as the template
kafka.outgoingEmailConsumerGroupId=outgoing-emails-consumer-group
# This is the dead letter topic for email that could not be sent
# The value of this config accord to the "kafka.outgoingEmailTopic" value.
# The pattern will be <value of kafka.outgoingEmailTopic>-dlt
kafka.errorSendingEmailTopic=outgoing-emails-dlt
# This is the group id of consumer that consumes the message from topic "kafka.errorSendingEmailTopic"
# You can enter any value for it or just leave it as the template
kafka.errorSendingEmailConsumerGroupId=error-sending-emails-consumer-group
logging.level.root=INFO
# You have to get the API from SparkPost, and enter to this
sparkpost.apiKey=<API key of SparkPost>
# You have to get the API from SparkPost, and enter to this
sendgrid.apiKey=<API key of SendGrid>
# The default client will be used to send the mail
# options: sparkpost, sendgrid
# If there is another email client, and it already implemented the EmailServiceClient interface
# and add to the EmailServiceProvider.emailClientRegistrar
# you cana use it here
emailclient.defaultEmailClient=sparkpost
logging.config=classpath:logback-spring.xml
```

- **Step 4:** Update variable `BASE_URL` in file `./Frontend/static/js/mailbox.js`. If you use docker compose from this
  project to start the service, you can set

```js
const BASE_URL = 'http://webservice:8080';
```

- **Step 5:** Update `server_name` in `Frontend/conf/emailservicesite.conf` file If you use docker compose from this
  project to start the service, you can set

```text
server_name webservice;
```

- **Step 6:** Update hostname, if your System Operation type is Linux or Unix you can change it in `/etc/hosts`

```text
127.0.0.1	webservice
```

- **Step 7:** Run docker compose

```shell
docker compose --env-file .env up --build -d
```

- **Step 8:** Check service status. `webservice` container depends on kafka, mysql and zookeeper are stable to start. It
  can take a couples minute. You can check it by access this address `webservice:8080/actuator/health`
  `webclient` container can be access by this address `localhost:8081`

- **Step 9:** Tracing logs for errors for `webservice` container. If there some error on the backend side, you can trace
  logs of its container using `traceId` from response to know the context of that error. The logs have been transfer to
  JSON format, so you can push to ELK stack for more convenience when tracing and search for the errors.  
  ![Log Tracking](design/architecture/LogTracing.png)

## Deployed services

- **Frontend:** `http://okeydokey.ml`
- **Backend:** `http://api.okeydokey.ml`

**Disclaim:**

- SparkPost can only send email from a verified domain. If you want to test the site, you have to use email with this
  domain `testing.widiland.com`. The email name is not strict, you can use any value. For
  instance: `hellothere@testing.widiland.com`
- Currently, SendGrid has some issue with their system, so the email can be delayed to 48 hours
- If you cannot see email in your inbox, please check spam box as well.

## Additional

- I have applied some design patter for this project, for instance:

**Strategy pattern**

**Interface (Strategy) for email clients**

```java
package com.phuonghp.emailservice.config.emailclient;

import com.phuonghp.emailservice.dto.EmailItemDto;

public interface EmailServiceClient {
    Boolean sendEmail(EmailItemDto emailItemDto);

    String getName();
}

```

**Context holder**

```java
package com.phuonghp.emailservice.emailprovider;

import com.phuonghp.emailservice.config.emailclient.EmailClientConfiguration;
import com.phuonghp.emailservice.config.emailclient.EmailServiceClient;
import com.phuonghp.emailservice.config.emailclient.SendGridClient;
import com.phuonghp.emailservice.config.emailclient.SparkPostClient;
import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.util.constant.EmailClientConstant;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class EmailServiceProvider {
    private EmailServiceClient emailServiceClient;
    private EmailItemDto emailItemDto;

    @Autowired
    private SendGridClient sendGridClient;

    @Autowired
    private SparkPostClient sparkPostClient;

    @Autowired
    private EmailClientConfiguration emailClientConfiguration;

    private Map<String, EmailServiceClient> emailClientRegistrar;

    public Boolean sendEmail() {
        return this.emailServiceClient.sendEmail(this.emailItemDto);
    }

    public void registerEmailClient() {
        this.emailClientRegistrar = new HashMap<>();
        this.emailClientRegistrar.put(EmailClientConstant.SENDGRID, this.sendGridClient);
        this.emailClientRegistrar.put(EmailClientConstant.SPARKPOST, this.sparkPostClient);
    }

    public EmailServiceClient getDefaultEmailClient() {
        return this.emailClientRegistrar.get(emailClientConfiguration.getDefaultEmailClient());
    }

    public EmailServiceClient getFallbackEmailClient() {
        Random r = new Random();
        var fallbackClients = this.emailClientRegistrar.keySet()
                .stream()
                .filter(emailClient -> !emailClientConfiguration.getDefaultEmailClient().equals(emailClient)).collect(Collectors.toList());
        if (!fallbackClients.isEmpty()) {
            int randomItem = r.nextInt(fallbackClients.size());
            String randomElement = fallbackClients.get(randomItem);
            return this.emailClientRegistrar.get(randomElement);
        }
        return null;
    }
}
```

**Concrete strategy**

```java
package com.phuonghp.emailservice.config.emailclient;

import com.phuonghp.emailservice.dto.EmailItemDto;
import com.phuonghp.emailservice.util.constant.EmailClientConstant;
import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Slf4j
@Getter
@Setter
@Configuration
@Validated
@ConfigurationProperties(prefix = "sparkpost")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SparkPostClient implements EmailServiceClient {

    @NotEmpty(message = "Missing api key for Spark Post")
    private String apiKey;
    private Client client;

    @Override
    public Boolean sendEmail(EmailItemDto emailItemDto) {
        try {
            this.client = new Client(this.apiKey);
            this.client.sendMessage(
                    emailItemDto.getSender(),
                    emailItemDto.getDirectRecipients(),
                    emailItemDto.getSubject(),
                    emailItemDto.getContent(),
                    emailItemDto.getContent());
            return true;
        } catch (SparkPostException e) {
            log.info("Error sending email: {}", emailItemDto);
            log.error("Error when sending email via Spark Post", e);
            return false;
        }
    }

    @Override
    public String getName() {
        return EmailClientConstant.SPARKPOST;
    }
}
```

**Client (Where we can change the strategy during runtime)**

```java
public class EmailService {
    public void sendEmailViaProvider(EmailItemDto emailItemDto) throws Exception {
        emailServiceProvider.registerEmailClient();
        var emailClient = emailServiceProvider.getDefaultEmailClient();
        emailServiceProvider.setEmailServiceClient(emailClient);
        emailServiceProvider.setEmailItemDto(emailItemDto);
        
        var isSuccessful = emailServiceProvider.sendEmail();
        if (Boolean.FALSE.equals(isSuccessful)) {
            emailClient = emailServiceProvider.getFallbackEmailClient();
            if (emailClient != null) {
                // This is where I change the strategy 
                emailServiceProvider.setEmailServiceClient(emailClient);
                emailServiceProvider.setEmailItemDto(emailItemDto);
                isSuccessful = emailServiceProvider.sendEmail();
            }
        }
        if (Boolean.TRUE.equals(isSuccessful)) {
            try {
                emailHistoryRepository.updateEmailClient(emailItemDto.getId(), emailClient.getName());
                return;
            } catch (Exception e) {
                log.error("Error occurred when saving client name that send the email", e);
                // Ignore this error due to the email has been sent"
                return;
            }
        }
        throw new CustomException.RetryableException("Retry sending email");
    }
}
```
