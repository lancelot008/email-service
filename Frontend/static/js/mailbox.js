const BASE_URL = 'https://api.okeydokey.ml';

const getInboxEmails = async () => {
    try {
        const response = await axios.get(`${BASE_URL}/emails/inbox?recipient=${email}`);
        const inboxMails = response.data.data;
        console.log(`GET: Here's the list of emails`, response);
        let inboxMailRows = ''
        for (const index in inboxMails) {
            const mail = inboxMails[index]
            inboxMailRows +=
                `<tr>
                    <td class="mailbox-name"><a href="#" onclick="getEmailDetails(${mail.id})">${mail.sender}</a></td>
                    <td class="mailbox-subject"><b>${mail.subject}</b> - ${mail.content}
                    </td>
                </tr>`
        }

        if (inboxMailRows.length === 0) {
            inboxMailRows = `<tr>
                    <td class="mailbox-name">Your inbox is empty</td>
                </tr>`;
        }

        document.getElementById("mailRows").innerHTML = inboxMailRows.toString();
        document.getElementById("inboxCounter").innerText = inboxMails.length;
    } catch (errors) {
        console.error(errors);
        alert("Cannot retrieve data from server");
    }
};

const getSentEmails = async () => {
    try {
        const response = await axios.get(`${BASE_URL}/emails/sent?sender=${email}`);
        const sentMails = response.data.data;
        console.log(`GET: Here's the list of emails`, response);
        let sentMailRows = ''
        for (const index in sentMails) {
            const mail = sentMails[index]
            sentMailRows +=
                `<tr">
                    <td class="mailbox-name"><a href="#" onclick="getEmailDetails(${mail.id})">${mail.sender}</a></td>
                    <td class="mailbox-subject"><b>${mail.subject}</b> - ${mail.content}
                    </td>
                    <td class="mailbox-date">${mail.status}</td>
                    <td class="mailbox-date">${mail.emailClient || '-'}</td>
                </tr>`
        }

        if (sentMailRows.length === 0) {
            sentMailRows = `<tr>
                    <td class="mailbox-name">Your sent box is empty</td>
                </tr>`;
        }

        document.getElementById("mailRows").innerHTML = sentMailRows.toString();
        document.getElementById("sentCounter").innerText = sentMails.length;
    } catch (errors) {
        console.error(errors);
        alert("Cannot retrieve data from server");
    }
};

const logout = () => {
    localStorage.removeItem("email");
    window.location.href = './lockscreen.html';
}


const sendEmail = async () => {
    const email = localStorage.getItem("email")
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    try {
        const recipientEmails = document.getElementById("recipientEmail").value
        const ccRecipientEmails = document.getElementById("ccRecipientEmail").value
        const bccRecipientEmails = document.getElementById("bccRecipientEmail").value

        const listRecipientEmails = !recipientEmails ? [] : recipientEmails.split(',')
        const listCcRecipientEmails = !ccRecipientEmails ? [] : ccRecipientEmails.split(',')
        const listBccRecipientEmails = !bccRecipientEmails ? [] : bccRecipientEmails.split(',')

        let emailItem = {
            "sender": email,
            "content": document.getElementById("emailContent").value,
            "subject": document.getElementById("emailSubject").value,
            "directRecipients": listRecipientEmails,
            "ccRecipients": listCcRecipientEmails,
            "bccRecipients": listBccRecipientEmails
        }

        const response = await axios.post(`${BASE_URL}/emails`, emailItem);
        console.log(response);
        $('#composeModal').modal('hide');
        Toast.fire({
            icon: 'success',
            title: 'Your email have been sent'
        });
    } catch (errors) {
        console.error("error", errors);
        const responseData = errors.response.data
        if (responseData) {
            console.error("responseData", responseData);
            const {errorCode, message} = responseData;
            console.error(errorCode, message);
            Toast.fire({
                icon: 'error',
                title: message || errorCode
            });
        }
    }
}


const getEmailDetails = async (id) => {
    try {
        const response = await axios.get(`${BASE_URL}/emails/${id}?email=${email}`);
        const emailDetails = response.data.data;
        console.log(`GET: Here's the details of email`, response);
        document.getElementById("emailDetailPlaceholder").innerHTML = `
        <div class="modal fade" id="emailDetailsModal">
            <div class="modal-dialog modal-dialog-scrollable modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Email details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body p-0">
                            <div class="mailbox-read-info">
                                <h5>${emailDetails.subject}</h5>
                                <h6>From: ${emailDetails.sender}
                                    <span class="mailbox-read-time float-right">${new Date(emailDetails.deliveryAt) || new Date(emailDetails.sentAt)}</span></h6>
                            </div>
                            <!-- /.mailbox-read-info -->
                            <div class="mailbox-read-message">
                                ${emailDetails.content}
                            </div>
                            <!-- /.mailbox-read-message -->
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
        `;
        $('#emailDetailsModal').modal('show');

    } catch (errors) {
        console.error(errors);
        alert("Cannot retrieve data from server");
    }
};


const switchMailBox = async (mailBox) => {
    const email = localStorage.getItem("email")
    if (mailBox === "inbox") {
        await getInboxEmails(email)
        document.getElementById("mailBoxHeader").innerText = "Inbox";
        document.getElementById("mailBoxBreadCrumb").innerText = "Inbox";
    } else if (mailBox === "sent") {
        await getSentEmails(email)
        document.getElementById("mailBoxHeader").innerText = "Sent";
        document.getElementById("mailBoxBreadCrumb").innerText = "Sent";
    }
}

function truncate(str, num) {
    if (str.length <= num) {
        return str
    }
    return str.slice(0, num) + '...'
}